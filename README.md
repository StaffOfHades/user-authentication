# User Authentication #

## Build & Run ##

```sh
$ cd user-authentication
$ sbt
> jetty:start
> browse
```

If `browse` doesn't launch your browser, manually open [http://localhost:8080/](http://localhost:8080/) in your browser.

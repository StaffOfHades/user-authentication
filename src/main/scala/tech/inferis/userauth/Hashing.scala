package tech.inferis.userauth

import java.security.MessageDigest

object Hashing {

   private lazy val sha256Instance = MessageDigest.getInstance("SHA-256")

   def sha256(input: String): String =
      sha256Instance.digest(input.getBytes("UTF-8")).map("%02x".format(_)).mkString
}

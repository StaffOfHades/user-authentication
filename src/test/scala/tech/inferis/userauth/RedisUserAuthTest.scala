package tech.inferis.userauth

import com.github.fppt.jedismock._

import com.redis.RedisClient

import org.scalatest._
import org.scalatra.test.scalatest._

import scala.util.{Try, Success, Failure}
import scala.language.implicitConversions

import tech.inferis.userauth._
import tech.inferis.userauth.db._
import tech.inferis.userauth.model._

trait RedisDB {
   var db: DB = null
   var r: RedisClient = null
   var redisServer: RedisServer = null

   def init = {
      redisServer = RedisServer.newRedisServer;
      redisServer.start
      r = new RedisClient(redisServer.getHost, redisServer.getBindPort)
      db = new Redis(r)
   }

   def deinit = {
      r.disconnect
      redisServer.stop
   }
}

abstract class RedisTest extends ExpandedFunSpec with RedisDB {

   override protected def beforeAll = {
      init
   }

   override protected def afterAll = {
      deinit
   }

}

abstract class RedisServletTest extends ExpandedScalatraSpec with RedisDB {

   override protected def beforeAll = {
      super.beforeAll
      init
   }

   override protected def afterAll = {
      deinit
      super.afterAll
   }

}

@Ignore
class RedisFuncionalityTest extends RedisTest {

   describe("Redis") {
      it("should allow a client to connect") {
         new RedisClient(redisServer.getHost, redisServer.getBindPort) should not equal (null)
      }
      it("should allow commands to be executed") {
         r.set("test", true) shouldBe true
         info("Setting arbitraty key")

         r.del("test")
      }
      it("should allow write and read") {
         Given("a element is added")
         r.set("test", false)

         Then("the key should exist")
         r.exists("test") shouldBe true

         And("the added value should exist for the given key")
         r.get("test").get shouldBe "false"

         r.del("test")
      }
      it("should allow delete") {
         Given("a value was added")
         r.set("test", false)

         And("then removed")
         r.del("test")

         Then("the key should not exist")
         r.exists("test") shouldBe false
      }
   }
}

//@Ignore
class RedisUserAuthTest extends RedisTest {

   describe("User authentication") {
      describe("when checking for user data") {
         it("should confirm a user exists") {
            Given("a user is added to the db")
            val user = User("hades", "password")
            db.registerUser(user)

            Then("the user should be exist in the db")
            db.validUser("hades") shouldBe true
         }
         it("should confirm a user does not exists") {
            Given("a user is added to the db")
            val user = User("zeus", "password")
            db.registerUser(user)

            And("a differet user should not exist in the db")
            db.validUser("poseidon") shouldBe false
         } 
         it("should confirm a password is correct for a user") {
            Given("a user is added to the db")
            val user = User("poseidon", "password")
            db.registerUser(user)

            When("checking if the correct password is valid for a user")
            val result = db.validPassword(user)

            Then("the password should return as valid")
            result shouldBe true
         }
         it("should confirm a password is incorrect for a user") {
            Given("a user is added to the db")
            var user = User("hera", "password")
            db.registerUser(user)

            When("checking if an incorrect password is valid for a user")
            user = User("hera", "drowssap")
            val result = db.validPassword(user)

            Then("the password should return as invalid")
            result shouldBe false
         }
         it("should confirm a password does not exist for a user") {
            Given("a user does not exit")

            When("checking if a password is valid for a user")
            val user = User("demeter", "password")
            val result = db.validPassword(user)

            Then("the password should return as invalid")
            result shouldBe false
         }
      }

      describe("when using ScalatraForm messages") {
         it("should show a message when the user is invalid") {
            Given("a user exists the db")
            val user = User("hestia", "password")
            db.registerUser(user)

            When("validating for another username")
            val message = db.validateUser("ares")
            
            Then("the correct message should return")
            message should not be None
            message.get should equal ("Invalid username")

            info(s""""${message.get}"""")
         }
         it("should show a message when the user password is incorrect") {
            Given("a user exists the db")
            var user = User("ares", "password")
            db.registerUser(user)

            When("validating an incorrect password for the username")
            user = User("ares", "drowssap")
            val message = db.validatePassword(user)
            
            Then("the correct message should return")
            message should not be None
            message.get should equal ("Invalid password")

            info(s""""${message.get}"""")
         }
      } 
   }
}

package tech.inferis.userauth

import java.security._
import java.net.HttpCookie
import javax.crypto._

import org.scalatest._
import org.scalatra.test.scalatest._

import pdi.jwt._
import pdi.jwt.exceptions._
import pdi.jwt.JwtJson._

import play.api.libs.json._

import scala.util.{Try, Success, Failure}

import tech.inferis.userauth._
import tech.inferis.userauth.model._

class UserAuthServletTest extends RedisServletTest with UserAuth {

   override protected def beforeAll = {
      super.beforeAll
      val guest = Hashing.sha256("guest")
      val guestPermissions = Hashing.sha256(s"$guest-permissions")
      r.hset("roles", "0", guest)
      r.lpush(guestPermissions, "/")
      r.lpush(guestPermissions, "/login")

      val roles = Map("user" -> List("/", "/home", "/login", "/logout"), "admin" -> List("/administrate"))

      var i = 1

      for ((role, permissions) <- roles) {
         val hashedRole = Hashing.sha256(role)
         r.hset("roles", i.toString, hashedRole)
         i = i << 1

         for (permission <- permissions) {
            val hashedRolePermissions = Hashing.sha256(s"$hashedRole-permissions")
            r.lpush(hashedRolePermissions, permission)
         }
      }
   }

   addServlet(new UserAuthServlet(db), "/*")

   def cookiesHeaderWith(cookies: Map[String, String]): Map[String, String] =
      Map("Cookie" -> (cookies.map { case (k, v) => new HttpCookie(k, v) } mkString "; "))

   describe("When not yet logged in") {
      it("should always redirect to login") {
         get("/home") {
            status shouldBe 302
         }
      }
   }

}

package tech.inferis.userauth

import javax.crypto._
import java.security._

import org.scalatest._
import org.scalatra.test.scalatest._

import pdi.jwt._
import pdi.jwt.exceptions._
import pdi.jwt.JwtJson._

import play.api.libs.json._

import scala.util.{Try, Success, Failure}

import tech.inferis.userauth._
import tech.inferis.userauth.model._

trait UserAuth extends SessionImplicits with JwtJsonValueImplicits {   
   implicit val parser: Parser = Base64Parser
   val keyGen = KeyGenerator.getInstance("HmacSHA256")
   val secRandom = new SecureRandom
   keyGen.init(secRandom)
   implicit val jwtKey = keyGen.generateKey
}

class JwtFuncionalityTest extends RedisTest with UserAuth {
   
   describe("JWT tokens") { 
      describe("when utilizing them") {
         it("should encode & decode a valid token") {
            Given("a claim containing the user &username")
            val claim = JwtClaim("""{"id": 1, "user": "hades"}""").toJsObject
            
            When("the claim is encoded using a key to generate a token")
            val algo = JwtAlgorithm.HS256
            val token = JwtJson.encode(claim, jwtKey, algo)
            
            Then("the token should be able to be decodified using that key")
            val (isSuccess, result) = JwtJson.decodeJson(token, jwtKey, Seq(algo)) match {
                case Success(v) => (true, v)
                case _ => (false, null)
            }
            isSuccess shouldBe true
            
            And("the claimed passed should be the same one recieved")
            result should equal (claim)
         }
         it("should encode & decode a valid token that includes a header") {
            Given("a claim containing the user &username")
            val claim = JwtClaim("""{"id": 1, "user": "hades"}""").toJsObject
         
            And("a header containing the algorithm")
            val algo = JwtAlgorithm.HS256
            val header = JwtHeader(algo).toJsObject

            When("the claim & header are encoded using a key to generate a token")
            val token = JwtJson.encode(header, claim, jwtKey)
            
            Then("the token should be able to be decodified using that key")
            val (isSuccess, head, result) = JwtJson.decodeJsonAll(token, jwtKey, Seq(algo)) match {
                case Success((h, c, _)) => (true, h, c)
                case _ => (false, null, null)
            }
            isSuccess shouldBe true
            
            And("the claim passed should be the same one recieved")
            result should equal (claim)

            And("the heading passed should be the same one received")
            head should equal (header)
         }
         it("should be unable to retrieve a token with an invalid key") {
            Given("a claim containing the user &username")
            val claim = JwtClaim("""{"id": 1, "user": "hades"}""").toJsObject
            
            When("the claim is encoded using a key to generate a token")
            val algo = JwtAlgorithm.HS256
            val token = JwtJson.encode(claim, jwtKey, algo)
            
            Then("the token should not be valid under another key")
            JwtJson.isValid(token, "wrongKey", Seq(algo))
            
            And("a JwtValidationException should be thrown")
            assertThrows[JwtValidationException] {
               JwtJson.validate(token, "wrongKey", Seq(algo))
            }
         }
         it("should be unable to retrieve an expired token") {
            Given("an expired claim containing the user &username")
            val claim = JwtClaim("""{"id": 1, "user": "hades"}""").issuedNow.expiresNow.toJsObject

            When("the claim is encoded using a key to generate a token")
            val algo = JwtAlgorithm.HS256
            val token = JwtJson.encode(claim, jwtKey, algo)

            Then("the token should not be valid due to expiration")
            JwtJson.isValid(token, jwtKey, Seq(algo))
            
            And("a JwtExpirationException should be thrown")
            assertThrows[JwtExpirationException] {
               JwtJson.validate(token, jwtKey, Seq(algo))
            }
         }
      }
      describe("when creating sessions from a UserSession") {
         it("should geneate a valid session token for anusername") {
            Given("a ActiveSession with itsusername, ip & available permissions")
            val sessionUser = ActiveSession("zeus", "localhost", List("/home", "/"))

            And("a JwtHeader containing the encryption alogorithm")
            val header = JwtHeader(JwtAlgorithm.HS256).toJsObject

            When("a session token is generated")
            val (token, _) = JwtSession.generateSession(sessionUser, header)

            Then("the token should be valid")
            JwtJson.isValid(token, jwtKey) shouldBe true
         }
         it("should generate a valid session token & convert it back into a UserSession ") {
            Given("a session token generated from a ActiveSession with itsusername, ip & available permissions")
            val sessionUser = ActiveSession("hades", "localhost", List("/home", "/"))
            val header = JwtHeader(JwtAlgorithm.HS256).toJsObject
            val (token, _) = JwtSession.generateSession(sessionUser, header)

            When("the JwtClaim is decoded")
            val result = JwtSession.decodeSession(token)

            Then("a valid ActiveSession should be decoded")
            result should not be None

            And("the ActiveSession recieved should match the SessionUser given")
            result.get shouldBe sessionUser
         }
      }
      describe("when managing session in the datebase") {
         it("should be able to generate & save a session for anusername") {
            Given("a valid ActiveSession")
            val username = "zeus"
            val hashedUsername = Hashing.sha256(username) 
            val ip = "localhost"
            val sessionUser = ActiveSession(username, ip, List("/home", "/"))

            When("its saved into a db")
            val token = db.saveSession(sessionUser, 10)

            Then("the correct ActiveSessions should return from the db")
            val savedSession = db.getSession(hashedUsername, ip)
            savedSession should not be None
            savedSession.get should equal (sessionUser)

            And("it should exist in the db")
            r.exists(token) shouldBe true
         }
         it("should be able to end a session for anusername") {
            Given("a session exists in the db")
            val username = "hades"
            val hashedUsername = Hashing.sha256(username) 
            val ip = "localhost"
            val sessionUser = ActiveSession(username, ip, List("/home", "/"))
            val token = db.saveSession(sessionUser, 10)
            val existingSession = db.getSession(hashedUsername, ip)
            existingSession should not be None

            When("that session is ended")
            val removedSession = db.endSession(hashedUsername, ip)
            removedSession should not be None

            Then("that session should not exist as an active session")
            val savedSession = db.getSession(hashedUsername, ip)
            savedSession shouldBe None
         }
         it("should be able to keep mutiple active sessions for the sameusername") {
            Given("different sessions generated & saved for a user with different ips")
            val username = "poseidon"
            val hashedUsername = Hashing.sha256(username)
            val ips = List("192.168.0.1", "192.168.1.0", "192.168.0.0", "192,168.1.1")
            val permissions = List("/home", "/")
            var sessionUsers = List.empty[ActiveSession]
            for(ip <- ips) {
               sessionUsers = ActiveSession(username, ip, permissions) :: sessionUsers 
            }
            
            When("they are saved into a db")
            var tokens = List.empty[String]
            for (session <- sessionUsers) {
               tokens = db.saveSession(session, 10) :: tokens
            }

            Then("valid sessions should return from the db")
            var savedSessions = List.empty[ActiveSession]
            for (ip <- ips) {
               val session = db.getSession(hashedUsername, ip)
               session should not be None
               savedSessions = session.get :: savedSessions
            }
            
            And("they should exist in the db")
            for (token <- tokens) {
               r.exists(token) shouldBe true
            }
         }
         it("should be able to end a single session for a user with multiple active sessions") {
            Given("multiple sessions exists for a user")
            val username = "hera"
            val hashedUsername = Hashing.sha256(username)
            val ips = List("192.168.0.1", "192.168.1.0", "192.168.0.0", "192,168.1.1")
            val sessionToRemove = 0
            val permissions = List("/home", "/")
            var sessionUsers = List.empty[ActiveSession]
            for(ip <- ips) {
               sessionUsers = ActiveSession(username, ip, permissions) :: sessionUsers 
            }
            var tokens = List.empty[String]
            for (session <- sessionUsers) {
               tokens = db.saveSession(session, 10) :: tokens
            }
            var existingSessions = List.empty[ActiveSession]
            for (ip <- ips) {
               val session = db.getSession(hashedUsername, ip)
               session should not be None
               existingSessions = session.get :: existingSessions
            }

            When("a single session is ended")
            val removedSession = db.endSession(hashedUsername, ips(sessionToRemove))
            removedSession should not be None

            Then("that session should not exist as an active session")
            val savedSession = db.getSession(hashedUsername, ips(sessionToRemove))
            savedSession shouldBe None

            And("the other sessions should remain as active")
            var savedSessions = List.empty[ActiveSession]
            for (ip <- ips) {
               if (ip != ips(sessionToRemove)) {
                  val session = db.getSession(hashedUsername, ip)
                  session should not be None
                  savedSessions = session.get :: savedSessions
               }
            }
         }
         it("should be able to send all sessions for a user with multiple active sessions") {
            Given("multiple sessions exists for a user")
            val username = "demeter"
            val hashedUsername = Hashing.sha256(username)
            val ips = List("192.168.0.1", "192.168.1.0", "192.168.0.0", "192,168.1.1")
            val permissions = List("/home", "/")
            var sessionUsers = List.empty[ActiveSession]
            for(ip <- ips) {
               sessionUsers = ActiveSession(username, ip, permissions) :: sessionUsers 
            }
            var tokens = List.empty[String]
            for (session <- sessionUsers) {
               tokens = db.saveSession(session, 10) :: tokens
            }
            var existingSessions = List.empty[ActiveSession]
            for (ip <- ips) {
               val session = db.getSession(hashedUsername, ip)
               session should not be None
               existingSessions = session.get :: existingSessions
            }

            When("all active sessions are terminated")
            val removedSessions = db.endSessions(hashedUsername)
            removedSessions.size should be > 0

            Then("no active sessions should exist for anusername")
            for (ip <- ips) {
              val session = db.getSession(hashedUsername, ip)
              session shouldBe None
            }
         }
      }
   }
}

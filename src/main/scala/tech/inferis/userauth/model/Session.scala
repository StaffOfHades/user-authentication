package tech.inferis.userauth.model

import play.api.libs.json._
import play.api.libs.functional.syntax._

case class LoginSession(username: String, ip: String)
case class ActiveSession(username: String, ip: String, permissions: List[String])

trait SessionImplicits {

   implicit val loginSessionJsonClaimReader: Reads[LoginSession] = (
      (JsPath \ "username").read[String] and
      (JsPath \ "ip").read[String]
   )(LoginSession.apply _)

   implicit val loginSessionJsonClaimWriter: Writes[LoginSession] = (
      (JsPath \ "username").write[String] and
      (JsPath \ "ip").write[String]
   )(unlift(LoginSession.unapply))

   implicit val activeSessionJsonClaimReader: Reads[ActiveSession] = (
      (JsPath \ "username").read[String] and
      (JsPath \ "ip").read[String] and
      (JsPath \ "permissions").read[List[String]]
   )(ActiveSession.apply _)
   
   implicit val activeSessionJsonClaimWriter: Writes[ActiveSession] = (
      (JsPath \ "username").write[String] and
      (JsPath \ "ip").write[String] and
      (JsPath \ "permissions").write[List[String]]
   )(unlift(ActiveSession.unapply))
   
}

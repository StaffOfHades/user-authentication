package tech.inferis.userauth

import org.junit.runner.RunWith

import org.scalatest._
import org.scalatest.junit.JUnitRunner
import org.scalatra.test.scalatest._

@RunWith(classOf[JUnitRunner])
trait ExpandedFunSpec extends FunSpec with Matchers with BeforeAndAfterAll with BeforeAndAfterEach with GivenWhenThen
trait ExpandedScalatraSpec extends ScalatraSpec with BeforeAndAfterEach with GivenWhenThen

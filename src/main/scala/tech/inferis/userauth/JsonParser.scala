package tech.inferis.userauth

import pdi.jwt._
import pdi.jwt.JwtJson._

import play.api.libs.json._

import scala.language.implicitConversions

class JwtClaimConvertor(val claim: JwtClaim) {
   def toJsObject = claim.toJsValue.asInstanceOf[JsObject]
   def toJsArray = claim.toJsValue.asInstanceOf[JsArray]
}

class JwtHeaderConvertor(val header: JwtHeader) {
   def toJsObject = header.toJsValue.asInstanceOf[JsObject]
   def toJsArray = header.toJsValue.asInstanceOf[JsArray]
}

trait JwtJsonValueImplicits {
   implicit def convertJwtClaimToJs(claim: JwtClaim) = new JwtClaimConvertor(claim)
   implicit def convertJwtHeaderToJs(header: JwtHeader) = new JwtHeaderConvertor(header)
}

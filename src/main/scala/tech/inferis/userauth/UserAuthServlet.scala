package tech.inferis.userauth

import javax.crypto.SecretKey

import org.scalatra._
import org.scalatra.i18n.{I18nSupport, Messages}
import org.scalatra.forms.{Constraint, FormSupport, label, mapping, maxlength, minlength, required, text}

import pdi.jwt.{JwtAlgorithm, JwtHeader}

import tech.inferis.userauth.db._
import tech.inferis.userauth.model._

class UserAuthServlet(db: DB)(implicit val jwtKey: SecretKey) extends ScalatraServlet
   with FormSupport with I18nSupport with JwtJsonValueImplicits {

   val form = mapping(
      "username" -> label("User", text(required, maxlength(32), minlength(4), exists)),
      "password" -> label("Password", text(required, maxlength(64), minlength(8)))
   )(User.apply).verifying {
      value => db.validatePassword(User(value.username, value.password)) match {
         case Some(s: String) => Seq("password" -> s)
         case Some(_) => Seq("password" -> "Generic error. Contact your adminstrator")
         case None => Nil
      }
   }

   def exists: Constraint = new Constraint() {
		override def validate(name: String, value: String, message: Messages): Option[String] =
			db.validateUser(value)
	}
	
   get("/:permission") {
		views.html.permissions("/" + params("permission"))
	}

	get("/adminstrate") {
      val user = JwtSession.authenticateSession(request) match {
         case Some(session) => session.username 
         case None => "User"
      }
      views.html.adminstrate(user)
	}

	get("/logout") {
      cookies.get("current_user") match {
         case Some(hU) => db.endSession(hU, request.getRemoteAddr)
         case None => Unit
      }
      cookies.delete("current_user")
      cookies.delete("session_token")
		redirect("/login")
	}

	get("/login") {
      JwtSession.authenticateSession(request) match {
         case Some(session) if session.username != "guest" => redirect("/home")
         case _ => views.html.login() 
      }
	}	

	get("/home") {
      val user = JwtSession.authenticateSession(request) match {
         case Some(session) => session.username 
         case None => "User"
      }
      views.html.home(user)

	}

	post("/login") {
		validate(form)(
			errors => {
				BadRequest(views.html.login())
			},
			user => {
            val hashedUser = Hashing.sha256(user.username)
            val permissions = db.getPermissions(hashedUser)
            val session = ActiveSession(user.username, request.getRemoteAddr, permissions)
            val token = db.saveSession(session)
            cookies.update("session_token", token)
            cookies.update("current_user", hashedUser)
            redirect("/home")
			}
		)
	}

	get("/") {
		views.html.hello()
	}

   before("/:permission") {
      val permission = params("permission")
      JwtSession.authenticateSession(request) match {
         case Some(session) => db.validPermission(session, s"/$permission") match {
            case true => Unit
            case false => {
               Logger.instance.info(s"Invalid permission for: $permission")
               redirect("/login")
            }
         }
         case None => {
            Logger.instance.info(s"Not a valid active session")
            val permissions = db.getPermissions(0)
            val session = ActiveSession("guest", "*", permissions)
            val header = JwtHeader(JwtAlgorithm.HS256).toJsObject
            val (token, _) = JwtSession.generateSession(session, header)
            cookies.update("session_token", token)
            redirect("/login")
         }
      }
   }

}

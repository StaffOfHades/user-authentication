
import com.redis.RedisClient

import java.security._

import javax.crypto._
import javax.servlet.ServletContext

import org.scalatra._

import tech.inferis.userauth._
import tech.inferis.userauth.db._

class ScalatraBootstrap extends LifeCycle { 

	override def init(context: ServletContext) {
      val redis = new RedisClient("localhost", 6379) 
      Redis.initDB(redis)
      val db: DB = new Redis(redis)
      
      val keyGen = KeyGenerator.getInstance("HmacSHA256")
      val secRandom = new SecureRandom
      keyGen.init(secRandom)
      implicit val jwtKey = keyGen.generateKey

      context.mount(new UserAuthServlet(db), "/*")
   }
	
}

package tech.inferis.userauth.db

import java.security.Key

import javax.crypto.SecretKey

import tech.inferis.userauth.model._

trait DB {
   protected val defaultRoleFlag = 1
   protected val defaultSessionExpiration: Long = 7200

   def registerUser(user: User, role: Int = defaultRoleFlag): Unit

   def validUser(user: String): Boolean
	def validateUser(value: String): Option[String]

	def validPassword(user: User):  Boolean
	def validatePassword(user: User): Option[String]

   def getRoles(hashedUsername: String): List[String]
   
   def getPermissions(role: Int): List[String] 
   def getPermissions(hashedUsername: String): List[String]
	def getPermissions(roles: List[String]): List[String]

	def validPermission(hashedUsername: String, value: String): Boolean
   def validPermission(session: ActiveSession, value: String): Boolean
   def validPermission(hashedUsername: String, ip: String, value: String)(implicit key: SecretKey): Boolean

   def saveSession(session: ActiveSession, expiration: Long = defaultSessionExpiration)(implicit key: Key): String   
   def getSession(hashedUsername: String, ip: String)(implicit key: SecretKey): Option[ActiveSession]
   def endSession(hashedUsername: String, ip: String)(implicit key: SecretKey): Option[ActiveSession]
   def endSessions(hashedUsername: String)(implicit key: SecretKey): List[ActiveSession]
}

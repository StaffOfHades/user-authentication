val ScalatraVersion = "2.6.4"

organization := "tech.inferis"

name := "User Authentication"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.12.6"

resolvers += Classpaths.typesafeReleases

libraryDependencies ++= Seq(
	"org.scalatra" %% "scalatra" % ScalatraVersion,
	"org.scalatra" %% "scalatra-forms" % ScalatraVersion,
	"org.scalatra" %% "scalatra-scalatest" % ScalatraVersion % "test",
	"org.scalactic" %% "scalactic" % "3.0.5",
	"org.scalatest" %% "scalatest" % "3.0.5" % "test",
	"ch.qos.logback" % "logback-classic" % "1.2.3" % "runtime",
	"org.eclipse.jetty" % "jetty-webapp" % "9.4.9.v20180320" % "container",
	"javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
	"net.debasishg" %% "redisclient" % "3.9",
	"com.github.fppt" % "jedis-mock" % "0.1.13",
	"com.pauldijou" %% "jwt-play-json" % "2.1.0",
	"com.typesafe.play" %% "play-json" % "2.7.2",
)

enablePlugins(SbtTwirl)
enablePlugins(ScalatraPlugin)

scalacOptions += "-feature"

parallelExecution in Test := true
fork in Test := true

//logBuffered in Test := false
//testOptions in Test := Tests.Argument(TestFramework.ScalaTest, "-h", test-results")

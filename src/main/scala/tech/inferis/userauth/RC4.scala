package tech.inferis.userauth

import java.math.BigInteger
import java.util.Base64

trait Parser {
    def parse(bytes: Array[Byte]): String
    def parse(encoded: String): Array[Byte]
}

object RC4 {

    private val keySize = 256

    private def ksa(key: Array[Byte]): Array[Int] = {
        val S = Array.range(0, keySize)
        val T = Array.fill(keySize){0}
        for (i <- 0 until keySize) {
            T(i) = key(i % key.length)
        }
        
        var j = 0
        for (i <- 0 until keySize) {
            j = (j + S(i) + T(i % key.length)) % keySize
            val temp = S(i)
            S(i) = S(j)
            S(j) = temp
        }
        S
    }
    
    private def prga(S: Array[Int], n: Int): Array[Int] = {
        var i = 0
        var j = 0
        for( _ <- Array.range(0, n)) yield {
            i = (i + 1) % keySize
            j = (j + S(i)) % keySize
            val temp = S(i)
            S(i) = S(j)
            S(j) = temp
            val t = (S(i) + S(j)) % keySize
            S(t)
        }
    }
    
    private def rc4(value: Array[Byte], key: Array[Byte]): Array[Int] = {
        val n = value.length
        val k = prga(ksa(key), n)
        for (i <- Array.range(0, n))
            yield value(i) ^ k(i)
    }
    
    def encrypt(value: Array[Byte], key: Array[Byte]) = rc4(value, key)
    def decrypt(value: Array[Byte], key: Array[Byte]) = rc4(value, key)
    
    def encrypt(value: String, key: String): Array[Byte] = rc4(value.getBytes("UTF-8"), key.getBytes("UTF-8")).map(_.toByte)
    def decrypt(value: Array[Byte], key: String): String = new String(rc4(value, key.getBytes("UTF-8")).map(_.toByte), "UTF-8")
    
    def encrypt(value: String, key: String)(implicit parser: Parser): String = {
        parser.parse(encrypt(value, key))
    }
    def decrypt(value: String, key: String)(implicit parser: Parser): String = {
        decrypt(parser.parse(value), key)
    }
}

object HexParser extends Parser {

    def parse(bytes: Array[Byte]) = {
        val sb = new StringBuilder
        for (b <- bytes) {
            sb.append(String.format("%02x", Byte.box(b)))
        }
        sb.toString
    }
    
    def parse(encoded: String) =
        new BigInteger(encoded, 16).toByteArray
}

object Base64Parser extends Parser {

    def parse(bytes: Array[Byte])  =
        Base64.getEncoder().encodeToString(bytes)
    
    def parse(encoded: String) =
        Base64.getDecoder().decode(encoded)
}

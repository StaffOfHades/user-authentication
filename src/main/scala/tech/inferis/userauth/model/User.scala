package tech.inferis.userauth.model

case class User(username: String, password: String)

package tech.inferis.userauth

import java.security.Key
import java.util.UUID

import javax.crypto.SecretKey
import javax.servlet.http.{Cookie, HttpServletRequest}

import pdi.jwt._
import pdi.jwt.JwtJson._

import play.api.libs.json._

import scala.util.{Failure, Success}

import tech.inferis.userauth.model._

object JwtSession extends JwtJsonValueImplicits with SessionImplicits  {

   def generateSession(content: ActiveSession, header: JsObject, expiration: Long = 7200)(implicit key: Key): (String, String) =  {
      val uuid = UUID.randomUUID.toString
      val claim = JwtClaim(Json.stringify(Json.toJson(content))).withId(uuid).issuedNow.expiresIn(expiration).toJsObject
      val token = JwtJson.encode(header, claim, key)

      (token, uuid)
   }

   def decodeSession(token: String)(implicit key: SecretKey): Option[ActiveSession] =
      JwtJson.decodeJson(token, key) match {
         case Success(r) => r.validate[ActiveSession] match {
            case JsSuccess(s, _) => Some(s)
            case _ => None
         }
         case _ => None
      }

   def authenticateSession(request: HttpServletRequest)(implicit key: SecretKey): Option[ActiveSession] =
      Option(request.getCookies) match {
         case Some(cookies) => {
            val token = cookies.find((c: Cookie) => c.getName == "session_token")

            token match {
               case Some(t) => decodeSession(t.getValue)
               case None => {
                  val authFailure = AuthenticationFailure(request)
                  Logger.instance.warn("Applicacion cookie not found")
                  Logger.instance.info(authFailure.toString)

                  None
               }
            }
         }
         case None => {
            Logger.instance.debug("No cookies found")

            None
         }   
      }
}

case class AuthenticationFailure(request: HttpServletRequest) {
   
   override def toString =
      s"""AuthenticationFailure
      \tUser-Agent:\t${request.getHeader("User-Agent")}
      \tRequest URL:\t${request.getRequestURL.toString}
      \tRemote Address:\t${request.getRemoteAddr}
      )"""

}

package tech.inferis.userauth.db

import com.redis._

import java.security.Key

import javax.crypto.SecretKey

import pdi.jwt._

import tech.inferis.userauth.{Hashing, JwtJsonValueImplicits, JwtSession, Logger}
import tech.inferis.userauth.model._

class Redis(r: RedisClient) extends DB with JwtJsonValueImplicits {

   def registerUser(user: User, role: Int) = {
      val hashedUsername = Hashing.sha256(user.username)
      val hashedPassword = Hashing.sha256(user.password)
      r.hset(hashedUsername, "password", hashedPassword)
      r.hset(hashedUsername, "rolesFlag", role)
   }

   def validUser(user: String) = r.exists(Hashing.sha256(user))
   
   def validateUser(value: String) =
      validUser(value) match {
         case true => None
         case false => Some("Invalid username")
      }

   def validPassword(user: User) = {
      val hashedUsername = Hashing.sha256(user.username)
      val hashedPassword = Hashing.sha256(user.password)
      
      r.exists(hashedUsername) &&
      (r.hget(hashedUsername, "password") match {
         case Some(password) if hashedPassword == password => true
         case _ => false
      })
   }
  
   def validatePassword(user: User) = {
      val hashedUsername = Hashing.sha256(user.username)
      val hashedPassword = Hashing.sha256(user.password)
      
      r.exists(hashedUsername) match {
         case true => r.hget(hashedUsername, "password") match {
            case Some(password) if hashedPassword == password => None
            case Some(_) => Some("Invalid password")
            case None => Some("No saved password for user. Please contact your adminstrator")
         }
         case false => Some("No user information exists. Please contact your adiminstrator")
      }
   }

   def getRoles(hashedUsername: String) = {
      var roles: List[String] = List()
      
      if (r.exists(hashedUsername)) {

         val roleFlag: Option[Int] = r.hget(hashedUsername, "rolesFlag") match {
            case Some(string) => 
               try {
                  Some(string.toInt)
               } catch {
                  case _: java.lang.NumberFormatException => None
               }
            case None => None
         }

         if (!roleFlag.isEmpty) {
            roles = unpackRolesFlag(roleFlag.get)
         } else {
            Logger.instance.debug(s"No roles found for: $hashedUsername")
         }
      } else {
         Logger.instance.warn(s"No user found")
      }

      roles
   }
   
   def getPermissions(role: Int) =
      r.hget("roles", role.toString) match {
         case Some(roleName) => {
            val hashedKey = Hashing.sha256(s"$roleName-permissions") 
            
            r.lrange(hashedKey, 0, -1) match {
               case Some(l) => l.flatten
               case None => List.empty[String]
            }
         }
         case None => List.empty[String]
      }
   
   def getPermissions(roles: List[String]) = {
      var permissions: List[String] = List()
      
      for (role <- roles) {
         val hashedKey = Hashing.sha256(s"$role-permissions") 
         r.lrange(hashedKey, 0, -1) match {
            case Some(l) => {
               permissions = permissions ::: l.flatten
            }
            case None => Unit
         }
      }
      
      permissions
   }

   def getPermissions(hashedUsername: String) = getPermissions(getRoles(hashedUsername))

   private def unpackRolesFlag(flag: Int): List[String] = {
      var roles: List[String] = List()
      var finished = flag <= 0
      var byte = 1

      while (!finished) {
         val role = flag & byte
         if (role > 0) {
            roles = r.hget("roles", role.toString) match {
               case Some(r) => r :: roles
               case None => roles
            }
         }
         byte = byte << 1
         finished = byte > 4
      }

      roles
   }

   def validPermission(hashedUsername: String, value: String) = getPermissions(hashedUsername).contains(value)

   def validPermission(session: ActiveSession, value: String) = session.permissions.contains(value)

   def validPermission(hashedUsername: String, ip: String, value: String)(implicit key: SecretKey) =
      getSession(hashedUsername, ip) match {
         case Some(session) => validPermission(session, value)
         case None => false
      }

   def saveSession(session: ActiveSession, expiration: Long)(implicit key: Key) = {
      val header = JwtHeader(JwtAlgorithm.HS256).toJsObject
      val (token, _) = JwtSession.generateSession(session, header, expiration)
      val hashedUsername = Hashing.sha256(session.username)
      val sessionsKey = Hashing.sha256(s"active-sessions-$hashedUsername") 
      val hashedIP = Hashing.sha256(session.ip.toString) 

      r.setex(token, expiration, hashedIP)
      r.lpush(sessionsKey, token)

      token
   }

   def getSession(hashedUsername: String, ip: String)(implicit key: SecretKey) =
      getSessionToken(hashedUsername, ip) match {
         case Some(token) => JwtSession.decodeSession(token)
         case None => None
      }

   private def getSessionToken(hashedUsername: String, ip: String): Option[String] = {
      val sessionsKey = Hashing.sha256(s"active-sessions-$hashedUsername") 
      val hashedIP = Hashing.sha256(ip.toString) 
      var sessionToken: Option[String] = None

      if (r.exists(sessionsKey)) {
         val sessions = r.lrange(sessionsKey, 0, -1) match {
            case Some(l) => l.flatten
            case None => {
               Logger.instance.warn("Unable to retrieve list of active sessions")
               List[String]()   
            }
         }

         var i = 0
         var found = false
         var token = ""
         while(!found && i < sessions.size) {
            token = sessions(i)
            if (r.exists(token)) {
               found = r.get(token).get == hashedIP
            } else {
               Logger.instance.info("Removing expired token")
               r.lrem(sessionsKey, 0, token)
            }
            i = i + 1
         }

         if (found) {
            sessionToken = Some(token)
         }
      } else {
         Logger.instance.debug("No active sessions found for id: $id")
      }

      sessionToken
   }

   def endSession(hashedUsername: String, ip: String)(implicit key: SecretKey) =
      getSessionToken(hashedUsername, ip) match {
         case Some(token) => {
            val sessionsKey = Hashing.sha256(s"active-sessions-$hashedUsername")
            r.lrem(sessionsKey, 0, token)
            r.del(token)
            JwtSession.decodeSession(token)
         }
         case None => None
      }

   def endSessions(hashedUsername: String)(implicit key: SecretKey) = {
      val sessionsKey = Hashing.sha256(s"active-sessions-$hashedUsername") 
      var sessionUsers = List.empty[ActiveSession]

      if (r.exists(sessionsKey)) {
         val sessions = r.lrange(sessionsKey, 0, -1) match {
            case Some(l) => l.flatten
            case None => {
               Logger.instance.warn("Unable to retrieve list of active sessions")
               List[String]()   
            }
         }

         for (token <- sessions) {
            if (r.exists(token)) {
              sessionUsers = JwtSession.decodeSession(token) match {
                case Some(s) => s :: sessionUsers
                case None => sessionUsers
              } 
              r.del(token)
            }
         }
         r.del(sessionsKey)
      } else {
         Logger.instance.debug(s"No active sessions found for id: $hashedUsername")
      }

      sessionUsers
   }

}

object Redis {

   def initDB(r: RedisClient): Unit = {
      val guest = Hashing.sha256("guest")
      val guestPermissions = Hashing.sha256(s"$guest-permissions")
      r.hset("roles", "0", guest)
      r.lpush(guestPermissions, "/")
      r.lpush(guestPermissions, "/login")

      val roles = Map("user" -> List("/", "/home", "/login", "/logout"), "admin" -> List("/administrate"))

      var i = 1

      for ((role, permissions) <- roles) {
         val hashedRole = Hashing.sha256(role)
         r.hset("roles", i.toString, hashedRole)
         i = i << 1

         for (permission <- permissions) {
            val hashedRolePermissions = Hashing.sha256(s"$hashedRole-permissions")
            r.lpush(hashedRolePermissions, permission)

         }  

      } 
   }

}
